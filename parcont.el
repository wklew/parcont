;;; parcont.el --- Continuation-based sexp editing commands -*- lexical-binding: t -*-

;; Copyright (C) 2021, 2022 Walter Lewis

;; Author: Walter Lewis <wklew@mailbox.org>
;; Keywords: lisp
;; Url: https://git.sr.ht/~wklew/parcont
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides a set of context-based commands for editing
;; s-expressions.  The idea of context is borrowed from the notion of
;; *continuation* in computer science.  A continuation represents "the
;; rest of the program" relative to some local expression.  The
;; ability to capture and restore the current continuation (possibly
;; up to some enclosing delimiter, in the case of delimited
;; continuations) makes possible a range of interesting computational
;; effects, such as exceptions, nondeterminism and dynamic state.

;; The same concept is applied here to editing of source code. The
;; commands `parcont-kill-context' and `parcont-yank' are used to
;; capture and restore the immediate context of an s-expression,
;; similarly to a program which captures and restores its
;; continuation.  Along with `parcont-yank-pop', these commands define
;; a contextual analog to the build-in killing and yanking interface.

;; We claim that most interesting structural editing commands can be
;; defined succinctly in terms of capturing and restoring the context
;; of an expression. For example, lispy and paredit's `convolute-sexp'
;; can be expressed as:

;;     (parcont-kill-context)
;;     (backward-up-list)
;;     (parcont-yank)

;; This sequence could be defined as a new command, or executed
;; manually as a series of three commands.  Either way, the solution
;; is composed of simple operations which are easy to reason about.

;;; Code:

(require 'parlib)

(defconst parcont-kill-ring-max 60
  "Maximum size of `parcont-kill-ring'.")

(defvar parcont-kill-ring (make-ring parcont-kill-ring-max)
  "Ring of killed continuations, analogous to `kill-ring'.")

;; Remember last yank so we can undo it for `parcont-yank-pop'

(defvar parcont-start nil)
(defvar parcont-end nil)
(defvar parcont-mark nil)
(defvar parcont-count nil)

;; Locating sexp bounds

;; Ring interface, works just like the regular kill ring

(defun parcont-kill-new (before after)
  (ring-insert parcont-kill-ring (cons before after)))

(defun parcont-kill-wrap (before after)
  (let ((cont (ring-remove parcont-kill-ring 0)))
    (ring-insert parcont-kill-ring
                 (cons (concat before (car cont))
                       (concat (cdr cont) after)))))

(defun parcont-kill-rotate (n)
  (while (> n 0)
    (ring-insert-at-beginning parcont-kill-ring
                              (ring-remove parcont-kill-ring 0))
    (setq n (- n 1))))

;; Clear the kill ring manually

(defun parcont-clear ()
  "Clear `parcont-kill-ring'."
  (interactive)
  (setq parcont-kill-ring (make-ring parcont-kill-ring-max)))

;; Core kill/yank commands

(defun parcont-kill-context (n)
  "Kill the context around N sexps at point.
The killed context is pushed onto `parcont-kill-ring', or
appended to the most recent entry if invoked immediately after a
previous call to `parcont-kill-context'."
  (interactive "*p")
  (let* ((bnd (parlib-sexp-bounds n))
         (sexps (buffer-substring (car bnd) (cdr bnd))))
    (backward-up-list)
    (let* ((cont-start (point))
           (cont-end (scan-sexps (point) 1))
           (before (buffer-substring cont-start (car bnd)))
           (after (buffer-substring (cdr bnd) cont-end)))
      (delete-region cont-start cont-end)
      (insert sexps)
      (indent-region cont-start (point))
      (goto-char cont-start)
      (if (eq last-command 'parcont-kill-context)
          (parcont-kill-wrap before after)
        (parcont-kill-new before after)))))

(defun parcont-yank (n)
  "Yank the most recent killed context around N sexps at point."
  (interactive "*p")
  (let* ((bnd (parlib-sexp-bounds n))
         (size (- (cdr bnd) (car bnd)))
         (cont (ring-ref parcont-kill-ring 0)))
    (push-mark (goto-char (car bnd)))
    (insert (car cont))
    (let* ((hole-start (point))
           (hole-end (+ hole-start size)))
      (goto-char hole-end)
      (insert (cdr cont))
      (setq parcont-start (car bnd)
            parcont-end (point)
            parcont-mark hole-end
            parcont-count n)
      (goto-char hole-start))))

;; Cycle through the recent kills, just like regular `yank-pop'

(defun parcont-yank-pop (n)
  "Replace the last yanked context with the Nth next killed."
  (interactive "*p")
  (unless (eq last-command 'parcont-yank)
    (user-error "Previous command was not `parcont-yank'"))
  (setq this-command 'parcont-yank)
  (delete-region parcont-mark parcont-end)
  (delete-region parcont-start (point))
  (parcont-kill-rotate n)
  (parcont-yank parcont-count))

;; Key bindings are shifted versions of the regular kill/yank bindings

(defun parcont-define-keys (map)
  (define-key map (kbd "C-S-w") #'parcont-kill-context)
  (define-key map (kbd "C-S-y") #'parcont-yank)
  (define-key map (kbd "M-Y") #'parcont-yank-pop))

(parcont-define-keys emacs-lisp-mode-map)
(parcont-define-keys lisp-mode-map)
(with-eval-after-load 'scheme (parcont-define-keys scheme-mode-map))
(with-eval-after-load 'racket-mode (parcont-define-keys racket-mode-map))
(with-eval-after-load 'fennel-mode (parcont-define-keys fennel-mode-map))
(with-eval-after-load 'geiser (parcont-define-keys geiser-repl-mode-map))

(provide 'parcont)

;;; parcont.el ends here
